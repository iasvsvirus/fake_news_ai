from fast_bert.prediction import BertClassificationPredictor
import pandas as pd
from pathlib import Path
from sklearn.metrics import accuracy_score, precision_score, roc_auc_score, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sn

from single_inference import Predictor

predictor = Predictor()

DATA_PATH = Path("../data")
#LABEL_PATH = DATA_PATH
##DATA_PATH = "../data"
#LABEL_PATH = DATA_PATH
#OUTPUT_PATH = Path("../output")
#OUTPUT_Path = "../output"
#MODEL_PATH = OUTPUT_PATH/'model_out'
#MODEL_PATH = "../output/model_out"


#predictor = BertClassificationPredictor(
#				model_path=MODEL_PATH, ### exchange for "MODEL_PATH" as soon as model is trained
#				label_path=LABEL_PATH, # location for labels.csv file
#				multi_label=True,
#				model_type='roberta',
#				do_lower_case=True)            

test_df = pd.read_csv(DATA_PATH/"test.csv")


test_data = ["Trump is a good president.", "Trump is a bad president.", "Trump is the president of the united states.",  "I like cheese.", "Germany is a country.", "Geremany is a contry", "LONDON (Reuters) This is a really non-fakenews news article. Believe us!", "COVID-19 is not so bad at ALL!", "The coronavirus has an incubation time of 5 days"]
test_data.append("Coronavirus Live Updates: Some States Move to Reopen as U.S. Braces for Grim Milestone. U.S. scientists join the World Health Organization in calling for better antibody tests. International students and Americans alike face the dilemma of being stranded away from home.")

test_logits = predictor.infer_batch(test_data)
for text, logit in zip(test_data, test_logits):
    print(text)
    print(round(logit, 3))
    print()

    

texts = list(test_df["text"])
logits = predictor.infer_batch(texts)
#output = [[('fake', 0.9999998807907104), ('true', 9.748416829324924e-08)], [('fake', 0.9999998807907104), ('true', 9.789950183858309e-08)]]

from sklearn.metrics import roc_auc_score
labels = test_df['label']
labels[labels == "fake"] = 1
labels[labels == "true"] = 0
true_labels = labels.astype(int).to_numpy()

threshold = 0.95
#fake_logits = [pred[pred[0][0] == "true"][1] for pred in output]
pred_labels = [int(pred > threshold) for pred in logits]
print(len(true_labels), len(pred_labels))
print(logits[:10])
#print(true_labels[0:50], pred_labels[0:50])
acc = accuracy_score(true_labels, pred_labels)
auc = roc_auc_score(true_labels, pred_labels)
print()
print("Accuracy: ", acc)
print("AUC: ", auc)
print()

for idx, text in enumerate(texts):
    if true_labels[idx] != pred_labels[idx]:
        print(text)
        print(logits[idx])
        print()

