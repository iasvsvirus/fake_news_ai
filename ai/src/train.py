import os
import sys
import logging
import datetime
from pathlib import Path

from fast_bert.modeling import BertForMultiLabelSequenceClassification
from fast_bert.data_cls import BertDataBunch, InputExample, InputFeatures, MultiLabelTextProcessor, convert_examples_to_features
from fast_bert.learner_cls import BertLearner
from fast_bert.metrics import accuracy, fbeta, roc_auc

from box import Box
import apex
import torch


DATA_PATH = Path("../data")
LABEL_PATH = DATA_PATH
LOG_PATH = Path("../logs")
OUTPUT_PATH = Path("../output")

os.makedirs(LOG_PATH, exist_ok=True)
os.makedirs(OUTPUT_PATH, exist_ok=True)

args = Box({
    "log_path": LOG_PATH,
    "data_dir": DATA_PATH,
    "output_dir": OUTPUT_PATH,
    "max_seq_length": 512,
    "do_train": True,
    "do_eval": True,
    "do_lower_case": False,
    "train_batch_size": 24, #24 for distilroberta
    "eval_batch_size": 32,
    "learning_rate": 5e-5,
    "num_train_epochs": 3,
    "warmup_proportion": 0.0,
    "seed": 3,
    "fp16": True,
    "fp16_opt_level": "O1",
    "weight_decay": 0.0,
    "adam_epsilon": 1e-8,
    "max_grad_norm": 1.0,
    "max_steps": -1,
    "warmup_steps": 500,
    "logging_steps": 50,
    "eval_all_checkpoints": False,
    "overwrite_output_dir": True,
    "overwrite_cache": False,
    "multi_label": True,
    "model_name": 'distilroberta-base', #xlnet-base-cased',#distilroberta-base', #'roberta-base',
    "model_type": 'roberta'
})

torch.cuda.empty_cache()

device = torch.device('cuda')
if torch.cuda.device_count() > 1:
    args.multi_gpu = True
else:
    args.multi_gpu = False
#args.multi_gpu = False

run_start_time = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
logfile = str(LOG_PATH/'log-{}.txt'.format(run_start_time))


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    handlers=[
        logging.FileHandler(logfile),
        logging.StreamHandler(sys.stdout)
    ])

logger = logging.getLogger()

logger.info(args)


databunch = BertDataBunch(args['data_dir'], LABEL_PATH, args.model_name, train_file='clean_train.csv', val_file='clean_val.csv',
                          test_data='clean_test.csv',
                          text_col="text",
                          batch_size_per_gpu=args['train_batch_size'], max_seq_length=args['max_seq_length'], 
                          multi_gpu=args.multi_gpu, multi_label=args['multi_label'], model_type=args.model_type)


metrics = []
metrics.append({'name': 'train_acc', 'function': accuracy})
metrics.append({'name': 'roc_auc', 'function': roc_auc})
metrics.append({'name': 'fbeta', 'function': fbeta})

learner = BertLearner.from_pretrained_model(databunch, args.model_name, metrics=metrics, 
                                            device=device, logger=logger, output_dir=args.output_dir, 
                                            warmup_steps=args.warmup_steps,
                                            multi_gpu=args.multi_gpu, is_fp16=args.fp16, 
                                            multi_label=args['multi_label'], logging_steps=500)

learner.fit(args.num_train_epochs, args.learning_rate, validate=True)

#learner.validate()

learner.save_model()
