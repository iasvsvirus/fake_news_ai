import fairseq
import torch
from torch.util.data import TensorDataset, DataLoader, Dataset
from skorch import NeuralNetClassifier
from apex import amp
from pytorch_lamb import Lamb

data_path = "../data"
train_data_path = data_path + "clean_train.csv"
train_df = pd.read_csv(train_data_path)
train_data = list(train_df["text"])
train_labels = train_df["label"]
train_labels[train_labels["label"] == "fake"] = 1
train_labels[train_labels["label"] == "true"] = 0
train_labels = train_labels.to_numpy()


device = torch.device('cuda')

class StringDataset(Dataset):
    def __init__(self, inputs, labels):
        self.inputs = inputs
        self.labels = labels
        
    def __getitem__(self, idx):
        return self.inputs[idx], self.labels[idx]
    
    def __len__(self):
        return len(self.inputs)

class RobertaClassifier(torch.nn.Module):
    def __init(self, max_seq_len=512):
        self.max_seq_len = max_seq_len
        
        self.roberta = torch.hub.load('pytorch/fairseq', 'roberta.large')
        for param in self.roberta.parameters():
            param.requires_grad = False
            
        roberta.register_classification_head('fake', num_classes=2)
        
    def forward(self, tokens, mean_pred=True):
        preds = []
        for idx in range(0, len(tokens), self.max_seq_len):
            state = tokens[idx: idx + self.max_seq_len]
            batch_size = state.shape[0]
            pred = roberta.predict("fake", state)
            preds.append(preds)
            if not mean_pred:
                break
        return torch.mean(torch.stack(preds))
    
    def preprocess_string(string):
        """ Transforms string into tokens, making sure the length is a multiple of max_seq_len"""
        tokens = self.roberta.encode(string)
        remainder = len(tokens) % self.max_seq_len
        tokens.extend([2] * remainder)
        return tokens
            
    def preprocess_string_list(self, string_list):
        return [self.preprocess_string(string) for string in string_list]
        
    
roberta = RobertaClassifier().to(device)
train_data = roberta.preprocess_string_list(train_data)
dataset = StringDataset(train_data, train_labels)
loader = DataLoader(dataset, batch_size=16, pin_memory=True)
loss_func = torch.nn.CrossEntropyLoss(weight=None, size_average=None, ignore_index=-100, reduce=None, reduction='mean')
opt = Lamb(roberta.parameters())

num_epochs = 3
for epoch in range(num_epochs):
    for data, label in loader:
        data, label = data.to(device), label.to(device)
        
        pred = roberta(data, mean_pred=False)
        
        loss = loss_func(pred, label)
    
        opt.zero_grad()
        loss.backward()
        opt.step()
        
    torch.save({
        
    })
        
        

class LSTMRoberta(torch.nn.Module):
    def __init(self, approach="head"):
        self.approach = approach
        self.max_seq_len = 512
        
        self.roberta = torch.hub.load('pytorch/fairseq', 'roberta.large')
        for param in self.roberta.parameters():
            param.requires_grad = False
            
        if self.approach == "head":
            roberta.register_classification_head('embedding', num_classes=1024)
            
        self.lstm = torch.nn.LSTM(1024, 512)
        
    def forward(self, tokens):
        for idx in range(0, len(tokens), self.max_seq_len):
            state = tokens[idx: idx + self.max_seq_len]
            batch_size = state.shape[0]
            
            # Approach 1 - use large classification head logits as embedding: 
            features = roberta.predict('embedding', state)  # log probs
            
            # Approach 2 - use feature embeddings from second last layer and average over all 512 token embeddings:
            # features = roberta.extract_features(tokens)
            # features = features.mean(dim=1)
            
        
        
            

#roberta.eval()  # disable dropout (or leave in train mode to finetune)

tokens = roberta.encode('Hello world!')
assert tokens.tolist() == [0, 31414, 232, 328, 2]
assert roberta.decode(tokens) == 'Hello world!'


# Extract the last layer's features
#last_layer_features = roberta.extract_features(tokens)
#assert last_layer_features.size() == torch.Size([1, 5, 1024])

# Extract all layer's features (layer 0 is the embedding layer)
#all_layers = roberta.extract_features(tokens, return_all_hiddens=True)
#assert len(all_layers) == 25
#assert torch.all(all_layers[-1] == last_layer_features)

roberta.register_classification_head('new_task', num_classes=3)
logprobs = roberta.predict('new_task', tokens)  # tensor([[-1.1050, -1.0672, -1.1245]], grad_fn=<LogSoftmaxBackward>)


net = NeuralNetClassifier(
    LSTMRoberta,
    max_epochs=3,
    lr=0.0003,
    # Shuffle training data on each epoch
    iterator_train__shuffle=True,
)

net.fit(X, y)
y_proba = net.predict_proba(X)
