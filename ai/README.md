# Transformer model for fake/real news classification

## Prepare dependencies

* create a conda env with Python 3.7
* install src/requirements.txt

## Required data structure

The train.py script requires the following datasets within the data folder:

* labels.csv - all possible labels
* train.csv
* val.csv
* test.csv

