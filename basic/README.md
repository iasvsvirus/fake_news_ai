#Basic model  using conventional techniques

## Prepare dependencies

* create a conda env with Python 3.7
* install src/requirements.txt

## Modules: 
 
* url-checker: returning True for Fake-News URL and False for non-Fake-News URL
* comment section activity checker