# -*- coding: utf-8 -*-

import csv
import os


def readFakeURLCSV():
    urls = []
    with open(
        os.getenv("FAKE_URLS_PATH", "../data/fake_urls.csv"),
        newline="",
        encoding="utf-8",
    ) as f:
        reader = csv.reader(f)
        for row in reader:
            urls.append(row)
    return urls


def cutUrl(url):
    start = url.find("www.")
    if start > -1:
        start += 4
    else:
        start = url.find("http://")
        if start > -1:
            start += 7
        else:
            start = url.find("https://")
            if start > -1:
                start += 8
            else:
                start = 0
    end = url.find("/", start)
    return url[start:end]


def checkUrl(url):
    urls = readFakeURLCSV()[0]
    if url in urls:
        return True
    else:
        cut = cutUrl(url)
        if cut in urls:
            return True
        else:
            return False
