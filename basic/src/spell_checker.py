# -*- coding: utf-8 -*-

from spellchecker import SpellChecker


def findTypos(text):
    for t in text:
        t.split(" ")
    spell = SpellChecker()
    misspelled = spell.unknown(text)
    return len(misspelled)


def checkSpelling(text):
    typos = findTypos(text)
    if typos > 70:
        return True
    else:
        return False
