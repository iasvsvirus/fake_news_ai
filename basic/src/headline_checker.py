# -*- coding: utf-8 -*-

def checkHeadline(text):
    if "?!" in text:
        return True
    else:
        uppercount = 0
        text.split(' ')
        for word in text:
            if word.isupper():
                uppercount += 1
        if uppercount > 2:
            return True
        return False