// var flag = 0;

DangerText = "This info looks fake!";
SuspiciousText = "This info looks suspicious!";
GoodText = "This info looks real!";

const checkColorClasses = {
	green: "check-green",
	red: "check-red"
}

const headlineTexts = {
	green: "Headline seems trustworthy",
	red: "Headline seems suspicious"
}

const urlTexts = {
	green: "URL seems trustworthy",
	red: "URL seems suspicious"
}

const spellingTexts = {
	green: "Spelling seems trustworthy",
	red: "Spelling seems suspicious"
}

const articleTextTexts = {
	green: "Text seems trustworthy",
	red: "Text seems suspicious"
}

function changeImage_Red()
{
	document.getElementById("virus").className = "virus-red";
	document.getElementById("speech").innerHTML = DangerText;
	// var button = document.getElementById("TextCheck");
	// button.style.backgroundColor = "#008000";
}

function changeImage_Orange()
{
	document.getElementById("virus").className = "virus-yellow";	
	document.getElementById("speech").innerHTML = SuspiciousText;
}

function changeImage_Green()
{
	document.getElementById("virus").className = "virus-green";
	document.getElementById("speech").innerHTML = GoodText;
	// var button = document.getElementById("TextCheck");
	// button.style.backgroundColor = "#008000";
}

function fadeInDetails() {
	document.getElementById("details").className = "";
}

function hideDetails() {
	document.getElementById("details").className = "hide";
}

function setHeadlineCheck(text, cssClass)
{
	setElementCheck("HeadlineCheck", text, cssClass);
}

function setURLCheck(text, cssClass)
{
	setElementCheck("URLcheck", text, cssClass);
}

function setSpellingCheck(text, cssClass)
{
	setElementCheck("SpellCheck", text, cssClass);
}

function setTextCheck(text, cssClass)
{
	setElementCheck("TextCheck", text, cssClass);
}


function setElementCheck(id, text, cssClass)
{
	var element = document.getElementById(id);
	element.innerHTML = text;
	setElementColor(element, cssClass);
}

function setElementColor(element, cssClass) {
	element.classList.remove(checkColorClasses.green);
	element.classList.remove(checkColorClasses.red);
	element.classList.add(cssClass);
}

// //change button colors
// var URLbutton = document.getElementById("URLcheck");
// URLbutton.style.backgroundColor = ""; //whatever color you need

// var ContentButton = document.getElementById("TextCheck");
// ContentButton.style.backgroundColor = "";

// var HeadlineButton = document.getElementById("HeadlineCheck");
// HeadlineButton.style.backgroundColor = "";

// var SpellingButton = document.getElementById("SpellCheck");
// SpellingButton.style.backgroundColor = "";