// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


document.addEventListener('DOMContentLoaded', function() {
    chrome.tabs.executeScript(null, {
        file: "js/readability_modified.js"
    });
});

chrome.runtime.onMessage.addListener(function(request, sender) {
    if (request.action == "getReadable") {
        //document.getElementById("proof-of-concept-title").innerHTML = request.source.title;
        //document.getElementById("proof-of-concept-excerpt").innerHTML = request.source.excerpt;
        let article = request.source;
        console.log(article);
        sendPostRequesToAI(article.uri, article.textContent, article.title);
    }
});

function sendPostRequesToAI(articleUrl, articleText, articleHealine) {
    let xhr = new XMLHttpRequest();
    let url = "http://35.189.241.4";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
        }
    };
    let data = JSON.stringify({'url': articleUrl,
        'text': articleText,
        'headline': articleHealine});
    xhr.send(data);
}
