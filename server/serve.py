from flask import Flask
from single_inference import Predictor
from flask_restful import Api, Resource, reqparse
from basic.src.headline_checker import checkHeadline
from basic.src.spell_checker import checkSpelling
from basic.src.url_checker import checkUrl
import os
import os.path


class PredictorAPI(Resource):
    def post(self):
        args = parser.parse_args()
        body = args["text"]
        url = args["url"]
        headline = args["headline"]
        ai_reponse = predictor.infer("{} {}".format(headline, body))
        url_response = checkUrl(url)
        spelling_response = checkSpelling("{} {}".format(headline, body))
        headline_response = checkHeadline(headline)
        return {
            "ai_response": ai_reponse,
            "url_response": url_response,
            "spelling_response": spelling_response,
            "headline_response": headline_response,
        }


def download_model():
    path = os.getenv("MODEL_PATH")
    if os.path.isfile(path + "/config.json"):
        return

    from google.cloud import storage

    # Instantiates a client
    storage_client = storage.Client()

    # The name for the new bucket
    bucket_name = "euvsvirus"
    bucket = storage_client.bucket(bucket_name)
    files = [
        "merges.txt",
        "pytorch_model.bin",
        "special_tokens_map.json",
        "tokenizer_config.json",
        "vocab.json",
        "config.json",
    ]

    for f in files:
        blob = bucket.blob("output/model_out/{}".format(f))
        blob.download_to_filename(str("{}/{}".format(os.getenv("MODEL_PATH"), f)))


if __name__ == "__main__":
    download_model()

    app = Flask(__name__)
    api = Api(app)
    predictor = Predictor()
    parser = reqparse.RequestParser()
    parser.add_argument("body", "url", "headline")
    api.add_resource(PredictorAPI, "/")

    app.run()
