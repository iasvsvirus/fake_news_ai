from fast_bert.prediction import BertClassificationPredictor
import pandas as pd
import operator
import os


class Predictor:
    def __init__(self, batch_size=256):
        self.batch_size = batch_size
        self.LABEL_PATH = os.getenv("LABEL_PATH", "data")
        self.MODEL_PATH = os.getenv(
            "MODEL_PATH", "output/model_out"
        )  # distilroberta-base as default for testing

        self.predictor = BertClassificationPredictor(
            model_path=self.MODEL_PATH,  ### exchange for "MODEL_PATH" as soon as model is trained
            label_path=self.LABEL_PATH,  # location for labels.csv file
            multi_label=True,
            model_type="roberta",
            do_lower_case=False,
        )

    def get_fake_scores(self, output):
        return [pred[pred[0][0] == "true"][1] for pred in output]

    def infer(self, input_text):
        output = self.predictor.predict(input_text)
        fake_score = self.get_fake_scores([output])[0]
        return fake_score

    def infer_batch(self, input_list):
        out = []
        for idx in range(0, len(input_list), self.batch_size):
            outputs = self.predictor.predict_batch(
                input_list[idx : idx + self.batch_size]
            )
            scores = self.get_fake_scores(outputs)
            out.extend(scores)
        return out
