FROM pytorch/pytorch:1.5-cuda10.1-cudnn7-runtime

COPY server server

COPY ai/data data

COPY basic server/basic

RUN pip install -r server/requirements.txt

ENV GOOGLE_APPLICATION_CREDENTIALS=./credentials.json

COPY credentials.json credentials.json

ENTRYPOINT [ "python3", "server/serve.py" ]